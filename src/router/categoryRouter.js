const express = require("express");
const router = express.Router();
const {
  getAllCategory,
  getCategoryById,
  createCategory,
  updateCategory,
  deletedCategory,
} = require("../controller/categoryController");
// const {
//   isRecruiter,
//   isWorkers,
//   LoginAuth,
// } = require("../middleware/verifyRole");
// const upload = require("../middleware/multer");

router.get("/", getAllCategory);
router.get("/:category_id", getCategoryById);
router.post("/", createCategory);
router.put("/:category_id", updateCategory);
router.delete("/:category_id", deletedCategory);

module.exports = router;

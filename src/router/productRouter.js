const express = require("express");
const router = express.Router();
const {
  getAllProduct,
  getProductById,
  createProduct,
  updateProduct,
  deletedProduct,
} = require("../controller/productController");
const upload = require("../middleware/multer");
// const {
//   isRecruiter,
//   isWorkers,
//   LoginAuth,
// } = require("../middleware/verifyRole");

router.get("/", getAllProduct);
router.get("/:product_id", getProductById);
router.post("/", upload, createProduct);
router.put("/:product_id", upload, updateProduct);
router.delete("/:product_id", deletedProduct);

module.exports = router;

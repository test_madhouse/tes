const express = require("express");
const router = express.Router();

const usersRouter = require("./usersRouter");
const categoryRouter = require("./categoryRouter");
const productRouter = require("./productRouter");

router.use("/users", usersRouter);
router.use("/category", categoryRouter);
router.use("/product", productRouter);

module.exports = router;

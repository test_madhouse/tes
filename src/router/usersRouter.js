const express = require("express");
const router = express.Router();
const {
  getAllUsers,
  getUsersById,
  deletedUsers,
  createUser,
  login,
} = require("../controller/usersController");
const { isAdmin, isUsers, LoginAuth } = require("../middleware/verifyRole");

router.get("/", LoginAuth, getAllUsers);
router.get("/:users_id", getUsersById);
router.post("/", createUser);
router.post("/login", login);
router.delete("/:users_id", deletedUsers);

module.exports = router;

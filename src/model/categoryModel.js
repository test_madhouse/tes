const db = require("../config/db");

const getCategory = () => {
  return db.query("SELECT * FROM category ");
};

const getCategoryId = (category_id) => {
  return db.query(
    `SELECT * FROM category WHERE category.category_id =${category_id}`
  );
};

const createdCategory = (data) => {
  const { name } = data;
  return new Promise((resolve, reject) =>
    db.query(`INSERT INTO category(name) VALUES('${name}')`, (err, res) => {
      if (!err) {
        resolve(res);
      } else {
        reject(err.message);
      }
    })
  );
};

const updateCategory = (name, category_id) => {
  return db.query(
    `UPDATE category SET name='${name}' WHERE category.category_id = ${category_id}`
  );
};

const deleteCategory = (category_id) => {
  return db.query(
    `DELETE FROM category WHERE category.category_id = ${category_id}`
  );
};

module.exports = {
  getCategory,
  getCategoryId,
  createdCategory,
  updateCategory,
  deleteCategory,
};

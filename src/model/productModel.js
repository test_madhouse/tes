const db = require("../config/db");

const getProduct = () => {
  return db.query("SELECT * FROM product ");
};

const getProductId = (product_id) => {
  return db.query(
    `SELECT * FROM product WHERE product.product_id =${product_id}`
  );
};

const createProduct = (data) => {
  const { name, price, image } = data;
  return new Promise((resolve, reject) => {
    db.query(
      `INSERT INTO product (name,price,image) 
        VALUES ('${name}','${price}','${image}')`,
      (err, res) => {
        if (!err) {
          resolve(res);
        } else {
          reject(err.message);
        }
      }
    );
  });
};

const updateProduct = (data, product_id) => {
  const { name, price, image } = data;
  return db.query(
    `UPDATE product SET name = '${name}', price = '${price}', image = '${image}' WHERE product.product_id=${product_id}`
  );
};

const deleteProduct = (product_id) => {
  return db.query(
    `DELETE FROM product WHERE product.product_id = ${product_id}`
  );
};

module.exports = {
  getProduct,
  getProductId,
  createProduct,
  updateProduct,
  deleteProduct,
};

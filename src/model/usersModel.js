const db = require("../config/db");

const getUsers = () => {
  return db.query("SELECT * FROM users ");
};

const getUsersId = (users_id) => {
  return db.query(`SELECT * FROM users WHERE users.users_id =${users_id}`);
};

const getUsersEmail = (email) => {
  return new Promise((resolve, reject) =>
    db.query(
      `SELECT * FROM users WHERE users.email = '${email}'`,
      (err, res) => {
        if (!err) {
          resolve(res);
        } else {
          reject(err.message);
        }
      }
    )
  );
};

const createdUsers = (data) => {
  const { email, password } = data;
  return new Promise((resolve, reject) =>
    db.query(
      `INSERT INTO users(email,password) VALUES('${email}', '${password}')`,
      (err, res) => {
        if (!err) {
          resolve(res);
        } else {
          reject(err.message);
        }
      }
    )
  );
};

const loginUser = (email) => {
  return new Promise((resolve, reject) => {
    db.query(`SELECT * FROM users WHERE email = '${email}'`, (err, res) => {
      if (err) return reject(err);
      resolve(res);
    });
  });
};

// const updateUsers = (name, users_id) => {
//     return db.query(`UPDATE users SET name='${name}' WHERE users.users_id = ${users_id}`);

// };

const deleteUsers = (users_id) => {
  return db.query(`DELETE FROM users WHERE users.users_id = ${users_id}`);
};

module.exports = {
  getUsers,
  getUsersId,
  createdUsers,
  deleteUsers,
  getUsersEmail,
  loginUser,
};

const {
  getCategory,
  getCategoryId,
  createdCategory,
  updateCategory,
  deleteCategory,
} = require("../model/categoryModel");
const cloudinary = require("../config/cloudinaryConfig");

const categoryController = {
  getAllCategory: async (req, res) => {
    try {
      let result = await getCategory();
      res.json({
        message: "get Category Succesfully",
        data: result.rows,
      });
    } catch (err) {
      res.json({
        error: err.message,
        message: "error get Category",
      });
    }
  },

  getCategoryById: async (req, res) => {
    try {
      let category_id = req.params.category_id;
      let result = await getCategoryId(category_id);
      res.json({
        message: "get Category Succesfully",
        data: result.rows,
      });
    } catch (err) {
      res.json({
        error: err.message,
        message: "error get Category",
      });
    }
  },

  createCategory: async (req, res) => {
    try {
      let data = {
        name: req.body.name,
      };

      const category = await createdCategory(data);

      res.json({
        data: category.rows,
        message: "Category created",
      });
    } catch (err) {
      res.json({
        message: "error creating Category",
        error: err.message,
      });
    }
  },

  updateCategory: async (req, res) => {
    try {
      const category_id = req.params.category_id;
      const category = {
        name: req.body.name,
      };
      //   console.log(category);
      const data = await updateCategory(category.name, Number(category_id));
      //   console.log(data);
      res.json({
        data: data.rows,
        message: "Category updated",
      });
    } catch (error) {
      res.json({
        error: error.message,
        message: "error updating Category",
      });
    }
  },

  deletedCategory: async (req, res) => {
    try {
      let category_id = req.params.category_id;
      let result = await deleteCategory(category_id);

      res.json({
        message: "deleted successfully",
        data: result.rows,
      });
    } catch (error) {
      res.json({
        message: "error delete Category",
        error: error.message,
      });
    }
  },
};

module.exports = categoryController;

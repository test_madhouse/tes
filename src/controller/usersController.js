const {
  getUsers,
  getUsersId,
  deleteUsers,
  getUsersEmail,
  createdUsers,
  loginUser,
} = require("../model/usersModel");
const { generateToken } = require("../helper/jwt");
const bcrypt = require("bcrypt");

const usersController = {
  getAllUsers: async (req, res) => {
    try {
      let result = await getUsers();
      res.json({
        message: "get Users Succesfully",
        data: result.rows,
      });
    } catch (err) {
      res.json({
        error: err.message,
        message: "error get users",
      });
    }
  },

  getUsersById: async (req, res) => {
    try {
      let users_id = req.params.users_id;
      let result = await getUsersId(users_id);
      res.json({
        message: "get Users Succesfully",
        data: result.rows,
      });
    } catch (err) {
      res.json({
        error: err.message,
        message: "error get users",
      });
    }
  },

  createUser: async (req, res) => {
    try {
      const { email, password } = req.body;

      let { rowCount } = await getUsersEmail(email);
      if (rowCount) {
        return res
          .status(400)
          .json({ message: "email already in use,please use another email" });
      }
      bcrypt.hash(password, 10, async (err, hash) => {
        if (err) {
          return res.status(500).json({
            message: "Error hashing password",
            error: err.message,
          });
        }

        const user = {
          email,
          password: hash,
        };
        // console.log(user);

        try {
          const userData = await createdUsers(user);

          res.status(200).json({
            message: "User has been created successfully",
            data: userData,
          });
        } catch (err) {
          console.error("Error creating user:", err);
          res.status(400).json({
            message: "Error creating user",
            err: err.message,
          });
        }
      });
    } catch (err) {
      res.status(400).json({
        message: "Error creating user Catch",
        err: err.message,
      });
    }
  },

  //   createUsers: async (req, res) => {
  //     try {
  //       let { email, password } = req.body;
  //       let data = {
  //         email: email,
  //         password: password,
  //       };
  //       const users = await createUsers(data.email, data.password);
  //       //   console.log(data);

  //       res.json({
  //         data: users.rows,
  //         message: "users created",
  //       });
  //     } catch (err) {
  //       res.json({
  //         message: "error creating users",
  //         error: err.message,
  //       });
  //     }
  //   },

  login: async (req, res) => {
    try {
      const { email, password } = req.body;
      const result = await loginUser(email);

      if (result.rowCount > 0) {
        const passwordHash = result.rows[0].password;
        const validPassword = await bcrypt.compare(password, passwordHash);
        const user = result.rows[0];
        if (validPassword) {
          const token = await generateToken({
            users: user,
          });
          console.log(token);

          res.status(200).json({
            message: "login succesful",
            user: user,
            token: token,
          });
        }
      }
    } catch (error) {
      res.json({
        message: "error login",
        error: error.message,
      });
    }
  },

  deletedUsers: async (req, res) => {
    try {
      let users_id = req.params.users_id;
      let result = await deleteUsers(users_id);

      res.json({
        message: "deleted successfully",
        data: result.rows,
      });
    } catch (error) {
      res.json({
        message: "error delete users",
        error: error.message,
      });
    }
  },
};

module.exports = usersController;

const {
  getProduct,
  getProductId,
  createProduct,
  updateProduct,
  deleteProduct,
} = require("../model/productModel");
const cloudinary = require("../config/cloudinaryConfig");

const productController = {
  getAllProduct: async (req, res) => {
    try {
      let result = await getProduct();
      res.json({
        message: "get Category Succesfully",
        data: result.rows,
      });
    } catch (err) {
      res.json({
        error: err.message,
        message: "error get Category",
      });
    }
  },

  getProductById: async (req, res) => {
    try {
      let product_id = req.params.product_id;
      let result = await getProductId(product_id);
      res.json({
        message: "get Category Succesfully",
        data: result.rows,
      });
    } catch (err) {
      res.json({
        error: err.message,
        message: "error get Category",
      });
    }
  },

  createProduct: async (req, res) => {
    try {
      let productImage = await cloudinary.uploader.upload(
        req.file && req.file?.path,
        {
          folder: "product",
        }
      );

      if (!productImage) {
        return res.json({ messsage: "need upload image" });
      }
      let product = {
        name: req.body.name,
        image: productImage.secure_url,
        price: req.body.price,
      };
      let productData = await createProduct(product);
      console.log(product);
      res.status(200).json({
        message: "create product succesfully",
        data: productData.rows,
      });
    } catch (err) {
      res.status(400).json({
        err: err.message,
        message: "error create products",
      });
    }
  },

  updateProduct: async (req, res) => {
    try {
      let product_id = req.params.product_id;
      let productImage = await cloudinary.uploader.upload(req.file.path, {
        folder: "product",
      });

      if (!productImage) {
        return res.json({ messsage: "need upload image" });
      }
      let product = await getProductId(Number(product_id));
      let data = product.rows[0];
      // console.log(data);
      let productData = {
        name: req.body.name || data.name,
        price: req.body.price || data.price,
        image: productImage.secure_url || data.image,
      };
      // console.log(productData);
      await updateProduct(productData, Number(product_id));
      res.status(200).json({
        message: "product updated successfully",
      });
    } catch (err) {
      res.status(400).json({
        error: err.message,
        message: "error update product",
      });
    }
  },

  deletedProduct: async (req, res) => {
    try {
      let product_id = req.params.product_id;
      let result = await deleteProduct(product_id);

      res.json({
        message: "deleted successfully",
        data: result.rows,
      });
    } catch (error) {
      res.json({
        message: "error delete Category",
        error: error.message,
      });
    }
  },
};

module.exports = productController;
